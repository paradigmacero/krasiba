// Variables globales
// Variables globales
// Variables globales
nombre      = document.getElementById("nombre");
email       = document.getElementById("email");
telefono    = document.getElementById("telefono");
empresa     = document.getElementById("empresa");
comentario  = document.getElementById("comentario");
patronEmail = /^[0-9a-z_\-\.]+@([a-z0-9\-]+\.?)*[a-z0-9]+\.([a-z]{2,4}|travel)$/i;




function validarContacto()
{                     
    if ( (nombre.value != "") && (email.value != "") && (telefono.value != "") && (empresa.value != "") && (comentario.value != "") )
    {
        if ( patronEmail.test(email.value) ) {return true;}
        else {alert("Tu email no es válido."); return false;}
    }
    else
    {
        alert("Aún restan campos por completar.");
        return false;
    }
}




function validarNewsletter()
{
    email2 = document.getElementById("email2");

    if (email2.value == "")
    {
      alert("Introduce tu email.");
      return false;
    }
    else
    {
      if ( patronEmail.test(email2.value) ) {return true;}
      else {alert("Tu email no es válido"); return false;}
    }
}



function validarPolitica()
{
    var politicaPrivacidad = document.getElementById("politicaPrivacidad");
    var nombre3            = document.getElementById("nombre3");
    var email3             = document.getElementById("email3");
    var puesto3            = document.getElementById("puesto3");
    var estado3            = document.getElementById("estado3");
    var ciudad3            = document.getElementById("ciudad3");
    var patronEmail        = /^[0-9a-z_\-\.]+@([a-z0-9\-]+\.?)*[a-z0-9]+\.([a-z]{2,4}|travel)$/i;

    if ( (nombre3.value != "") && (email3.value != "") && (puesto3.value != "") && (estado3.value != "") && (ciudad3.value != "") ) {
        if (politicaPrivacidad.checked) {
            if ( patronEmail.test(email3.value) ) {
                return true;
            }
            else {
                alert("Tu email no es válido"); return false;
                return false;
            }
        }
        else {
            alert("Antes debes aceptar nuestra Política de Privacidad");
            return false;
        }
    }
    else {
        alert("Los campos con asterisco son requeridos");
        return false;
    }
}




        

function soloNumeros(e)
{
    tecla = (document.all)?e.keyCode:e.which;
    if (tecla == 8) return true;
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
} 




function mayusMinus(e,f)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true;
    patron =/[\D\s]/;
    x = String.fromCharCode(tecla);

    if ( !patron.test(x) ) return false;

    txt = f.value;
    if (txt.length == 0 || txt.substr(txt.length-1,1) == " ")
    {
        f.value = txt+x.toUpperCase();
        return false;
    }
}


