<?php
    $nivelDePagina = 'index-out';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- <base href="http://campokrasiba.com.mx/dev/"> -->
    <?php include('../../base/includes/head-para-galerias.php'); ?>
    <script type="text/javascript">var nivelDePagina = 'index-out'; var pagina = 'galeria-individual';</script>
</head>
<body>
    <?php include '../../base/includes/gtm.php'; ?>

    <?php include('../../base/includes/preloader-para-galerias.php'); ?>
    <?php include('../../base/includes/menu-para-galerias.php'); ?>


<!-- START PAGE WRAPPER -->
<div id="page-wrapper">

    <!-- START WORK SECTION -->
    <section id="galeriaFotografica" class="section">

            <!-- START WRAPPER -->

            <div class="headline-wrapper contact-headline">
                <!-- START ROW -->
                <div class="row">
                    <h1 class="underline">
                        Chacón y Rodríguez
                    </h1>
                    <p class="undertitle">
                        21 de octubre
                    </p>
                </div><!-- END ROW -->
            </div><!-- END WRAPPER -->
            
            
            <!-- START PARALLAX CONTENT -->
            <div class="parallax-content parallax-contact">
                
                <!-- START ROW -->
                <div class="row">
                    <!-- START CONTACT FORM -->
                    <div class="contact-form">
                        
                        <div class="photoG">
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img01.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img01.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img02.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img02.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img03.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img03.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img04.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img04.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img05.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img05.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img06.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img06.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img07.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img07.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img08.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img08.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img09.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img09.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img10.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img10.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img11.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img11.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img12.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img12.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img13.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img13.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img14.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img14.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img15.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img15.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img16.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img16.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img17.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img17.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img18.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img18.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img19.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img19.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery49-img20.jpg" title="Chacón y Rodríguez, 21 de octubre">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery49-img20.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img regresar">
                                <a href="javascript:history.back(1)" title="Regreso a la página anterior">
                                    <img src="../../base/img/varias/regresar.png">
                                </a>
                            </div>
                        </div>

                    </div><!-- END CONTACT FORM -->
                </div><!-- END ROW -->
            </div><!-- END PARALLAX CONTENT -->
    </section><!-- END WORK SECTION -->
    
    






    <?php include("../../base/includes/footer-para-galerias.php"); ?>
    <?php include("../../base/includes/colorbox-para-galerias.php"); ?>
    


<!-- Start of Async HubSpot Analytics Code -->
 <script type="text/javascript">
   (function(d,s,i,r) {
     if (d.getElementById(i)){return;}
     var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
     n.id=i;n.src='skins/larry//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/475351.js';
     e.parentNode.insertBefore(n, e);
   })(document,"script","hs-analytics",300000);
 </script>
<!-- End of Async HubSpot Analytics Code -->

</body>
</html>