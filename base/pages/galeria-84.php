<?php
    $nivelDePagina = 'index-out';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- <base href="http://campokrasiba.com.mx/dev/"> -->
    <?php include('../../base/includes/head-para-galerias.php'); ?>
    <script type="text/javascript">var nivelDePagina = 'index-out'; var pagina = 'galeria-individual';</script>
</head>
<body>
    <?php include '../../base/includes/gtm.php'; ?>

    <?php include('../../base/includes/preloader-para-galerias.php'); ?>
    <?php include('../../base/includes/menu-para-galerias.php'); ?>


<!-- START PAGE WRAPPER -->
<div id="page-wrapper">

    <!-- START WORK SECTION -->
    <section id="galeriaFotografica" class="section">

            <!-- START WRAPPER -->

            <div class="headline-wrapper contact-headline">
                <!-- START ROW -->
                <div class="row">
                    <h1 class="underline">
                        Sanfer
                    </h1>
                    <p class="undertitle">
                        24 de Enero 2015
                    </p>
                </div><!-- END ROW -->
            </div><!-- END WRAPPER -->
            
            
            <!-- START PARALLAX CONTENT -->
            <div class="parallax-content parallax-contact" style="background:url(http://campokrasiba.com.mx/base/img/secciones/galeria/bg1.jpg); background-repeat:repeat-y;">
                
                <!-- START ROW -->
                <div class="row">
                    <!-- START CONTACT FORM -->
                    <div class="contact-form">
                        
                        <div class="photoG">
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img01.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img01.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img02.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img02.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img03.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img03.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img04.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img04.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img05.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img05.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img06.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img06.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img07.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img07.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img08.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img08.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img09.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img09.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img10.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img10.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img11.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img11.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img12.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img12.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img13.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img13.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img14.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img14.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img15.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img15.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img16.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img16.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img17.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img17.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img18.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img18.jpg" />
                                </a>
                            </div>
                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img19.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img19.jpg" />
                                </a>
                            </div>

                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img20.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img20.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img21.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img21.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img22.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img22.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img23.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img23.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img24.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img24.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img25.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img25.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img26.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img26.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img27.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img27.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img28.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img28.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img29.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img29.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img30.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img30.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img31.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img31.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img32.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img32.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img33.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img33.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img34.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img34.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img35.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img35.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img36.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img36.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img37.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img37.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img38.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img38.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img39.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img39.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img40.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img40.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img41.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img41.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img42.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img42.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img43.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img43.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img44.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img44.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img45.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img45.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img46.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img46.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img47.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img47.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img48.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img48.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img49.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img49.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img50.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img50.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img51.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img51.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img52.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img52.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img53.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img53.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img54.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img54.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img55.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img55.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img56.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img56.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img57.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img57.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img58.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img58.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img59.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img59.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img60.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img60.jpg" />
                                </a>
                            </div>


                            <div class="photoG-img">
                                <a class="group1" href="../../base/img/photo-album/galleries/gallery84-img61.jpg" title="Sanfer 24 de Enero 2015">
                                    <img src="../../base/img/photo-album/galleries/thumbs/gallery84-img61.jpg" />
                                </a>
                            </div>




                            <div class="photoG-img regresar">
                                <a href="javascript:history.back(1)" title="Regreso a la página anterior">
                                    <img src="../../base/img/varias/regresar.png">
                                </a>
                            </div>
                        </div>

                    </div><!-- END CONTACT FORM -->
                </div><!-- END ROW -->
            </div><!-- END PARALLAX CONTENT -->
    </section><!-- END WORK SECTION -->
    
    






    <?php include("../../base/includes/footer-para-galerias.php"); ?>
    <?php include("../../base/includes/colorbox-para-galerias.php"); ?>
    


<!-- Start of Async HubSpot Analytics Code -->
 <script type="text/javascript">
   (function(d,s,i,r) {
     if (d.getElementById(i)){return;}
     var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
     n.id=i;n.src='skins/larry//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/475351.js';
     e.parentNode.insertBefore(n, e);
   })(document,"script","hs-analytics",300000);
 </script>
<!-- End of Async HubSpot Analytics Code -->

</body>
</html>