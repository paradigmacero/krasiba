    <!-- META DATA -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  	<meta charset="utf-8" />
	  <title>Campo Krasiba - El lugar donde el "No puedo" no existe</title>
    <meta name="description" content="Integra el trabajo en equipo e incentiva el liderazgo en tu empresa con Métodos Krasiba"/>
    <meta name="keywords" content="team building, capacitacion vivencial, capacitacion, vivencial, team, building"/>
    <meta name="author" content="Campo Krasiba | Powered by Paradigma Cero"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="../../base/img/logos/favicon.ico" type="image/x-icon" />
	
    <!-- THEME STYLESHEETS -->
    <link rel="stylesheet" media="all" href="../../css/2015/reset.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" media="all" href="../../css/2015/responsive.gs.12col.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/jquery.maximage.css" />
	  <link rel="stylesheet" media="all" href="../../css/2015/jquery.bxslider.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/theme.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/shortcodes.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/responsive.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/color.css" />
    <link rel="stylesheet" media="all" href="../../css/2015/estilos-d.css" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" media="all" href="../../css/2015/ie-stylesheet.css" />
    <![endif]-->


    <!-- JAVASCRIPT PLUGINS -->
	<script type="text/javascript" src="../../scripts/jquery.1.9.1.min.js"></script>
	<script type="text/javascript" src="../../scripts/modernizr.custom.97074.js"></script>


    <style type="text/css">
    .rslides {
      position: relative;
      list-style: none;
      overflow: hidden;
      width: 100%;
      padding: 0;
      margin: 0;
      }

    .rslides li {
      -webkit-backface-visibility: hidden;
      position: absolute;
      display: none;
      width: 100%;
      left: 0;
      top: 0;
      }

    .rslides li:first-child {
      position: relative;
      display: block;
      float: left;
      }

    .rslides img {
      display: block;
      height: auto;
      float: left;
      width: 100%;
      border: 0;
      }
    </style>