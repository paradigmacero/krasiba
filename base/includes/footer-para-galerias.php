

    <!-- START FOOTER -->
    <footer id="footer">


		<!-- START ROW -->
        <div class="row gutters contact-row">
				
			<!-- START COLUMN 4/12 -->


        <!--  Aplicar posisiòn RELATIVA al siguiente SOCIAL-links, para que se ubique arriba de detalles de contacto-->
            <div class="col span_4">
                <div class="social-links">
                    <a href="https://plus.google.com/117417186499848167537/posts" target="_blank"><i class="fa fa-google-plus-square"></i></a>
                    <a href="https://twitter.com/CampoKrasiba" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="http://www.facebook.com/CampoKrasiba" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="http://www.youtube.com/user/campokrasiba" target="_blank"><i class="fa fa-youtube"></i></a>
                    <a href="http://mx.linkedin.com/pub/campo-krasiba/3a/39b/4a" target="_blank"><i class="fa fa-linkedin"></i></a>
                </div>
                
                <h4>Detalles de Contacto</h4>
				<!-- START CONTACT DETAILS -->
                <div class="contact-details">
                    <!-- START DETAIL -->
                    <span class="detail">
                        <span class="icon"><i class="fa fa-map-marker">&nbsp;&nbsp;&nbsp;Oficinas:</i></span>
                        <span class="text">
                          <br>
                            Río Danubio 113 - 902
                            <br>
                            Col. Cuauhtémoc C.P. 06500 
                            <br>
                            México, D.F.
                        </span>
                    </span><!-- END DETAIL -->
                    
                    <br>

                    <!-- START DETAIL -->
                    <span class="detail">
                        <span class="icon"><i class="fa fa-mobile">&nbsp;&nbsp;&nbsp;Teléfonos:</i></span>
                        <span class="text">
                            <br>
                            +52 (55) 5511 1554
                            <br>
                            5208 9691
                            <br>
                            5208 9425
                        </span>
                    </span><!-- END DETAIL -->

                    <!-- START DETAIL -->
                    <span class="detail">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text"><a href="javascript:;">Correo electrónico:</a></span>
                    </span><!-- END DETAIL -->

                    <!-- START DETAIL -->
                    <span class="detail">
                        <span class="icon"><i class="fa fa-link"></i></span>
                        <span class="text">
                            <a href="mailto:campokrasiba@gmail.com">campokrasiba@gmail.com</a> y
                            <br>
                            <a href="mailto:efuentes@campokrasiba.com.mx">efuentes@campokrasiba.com.mx</a>
                        </span>
                    </span><!-- END DETAIL -->                    
                </div><!-- END CONTACT DETAILS -->
                <br>
                <br>
                <br>
                <!-- <a href="reclutamiento.php">Bolsa de trabajo</a> -->
                <br>
                <a href="aviso-de-privacidad.html">Aviso de Privacidad</a>
                <br>
                <br>
                Inscríbete a nuestro newsletter 
            </div><!-- START COLUMN 4/12 -->

            <div style="text-align:center; margin-top:2rem;">
                <form id='formNewsletter' action="process.php" method="post" onsubmit="return validarNewsletter()">
                    <input id="email2" name="email2" type="text" class="text cleartext" placeholder="Tu email aquí" value="" style="height: 50px;" />
                    <input type="submit" class="submit" value="Suscribirse" id="submitFormaNewsletter" style="height: 50px; line-height: 0px;" />
                    <input type="text" name="address">
                    <input type="hidden" name="forma" value="formaNewsletter">
                </form>
                <br />
                <br />
                <!-- <h4><a href="reclutamiento.php">Reclutamiento de personal</a></h4> -->
            </div>

        </div><!-- END ROW -->
		
		
		<!-- START BOTTOM BOX -->
		<div class="bottom-box">
            <!-- START ROW -->
			<div class="row">
				<div align="center" itemscope="" itemtype="http://schema.org/Organization" style="position:relative; top:0px; left:0px;">
                      <a itemprop="url" href="http://www.paradigmacero.com" rel="follow" target="_blank" alt="Paradigma Cero, Desarrollo Web y Marketing Digital" title="Paradigma Cero, Desarrollo Web y Marketing Digital">
                         <img src="http://www.paradigmacero.net/sitios/logo-blanco.png" itemprop="logo" alt="Paradigma Cero, Desarrollo Web y Marketing Digital" height="20" border="0" style="width: 170px; margin: 12px;">
                      </a>
                      <br>
<!--                       
                      <a id="linkAP01" href="http://www.paradigmacero.com/" target="_blank" style="font-size: .7rem;">
                        ROI Marketing |
                      </a>
                      <a id="linkAP02" href="http://www.paradigmacero.com/" target="_blank" style="font-size: .7rem;">
                        Agencia marketing digital
                      </a> -->
                </div>
			</div><!-- END ROW -->
		</div><!-- END BOTTOM BOX -->
    </footer><!-- END FOOTER -->
	
</div><!-- END PAGE WRAPPER -->


<!-- JAVASCRIPT PLUGINS -->
<script type="text/javascript" src="../../scripts/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="../../scripts/SmoothScroll.js"></script>
<script type="text/javascript" src="../../scripts/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="../../scripts/jquery.cycle.all.js"></script>
<script type="text/javascript" src="../../scripts/jquery.maximage.js"></script>
<script type="text/javascript" src="../../scripts/jquery.bxslider.js"></script>
<script type="text/javascript" src="../../scripts/jquery.hoverdir.js"></script>
<script type="text/javascript" src="../../scripts/jquery.mixitup.js"></script>
<script type="text/javascript" src="../../scripts/jquery.fitvids.js"></script>
<!-- <script type="text/javascript" src="../../scripts/jquery.carouFredSel-6.2.1.js"></script> -->
<script type="text/javascript" src="../../scripts/respond.min.js"></script>


<!-- THEME JAVASCRIPT -->
<script type="text/javascript" src="../../scripts/theme.script.js"></script>

<!-- THEME MAIL -->
<script type="text/javascript" src="../../scripts/theme.mail.js"></script>

<!-- 
SETTINGS IN theme.settings.js:
 
	1. WORK PREVIEW HOVERDIR
	2. PARALLAX BACKGROUNDS
	3. SLIDER
    3.0 FULLSCREEN HEADER IMAGE SLIDER
    3.1 MACBOOK DEVICE SLIDER
    3.2 IPHONE DEVICE SLIDER
    3.3 COMMENT SLIDER
  4. CAROUSEL
    4.1 CLIENT CAROUSEL
-->
<script type="text/javascript" src="../../scripts/theme.settings.js"></script>
<script type="text/javascript" src="../../scripts/validator.js"></script>
<script type="text/javascript" src="../../scripts/JQBrowser.js"></script>


<!-- RESPONSIVESLIDES -->
<script src="../../scripts/responsiveslides.min.js"></script>
<script type="text/javascript">
    $(".rslides").responsiveSlides({
      auto: true,             // Boolean: Animate automatically, true or false
      speed: 500,            // Integer: Speed of the transition, in milliseconds
      timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
      pager: false,           // Boolean: Show pager, true or false
      nav: false,             // Boolean: Show navigation, true or false
      random: false,          // Boolean: Randomize the order of the slides, true or false
      pause: false,           // Boolean: Pause on hover, true or false
      pauseControls: true,    // Boolean: Pause when hovering controls, true or false
      prevText: "Previous",   // String: Text for the "previous" button
      nextText: "Next",       // String: Text for the "next" button
      maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
      navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
      manualControls: "",     // Selector: Declare custom pager navigation
      namespace: "rslides",   // String: Change the default namespace used
      before: function(){},   // Function: Before callback
      after: function(){}     // Function: After callback
    });

    $(".rslidesTestimonios").responsiveSlides({
      auto: true,             // Boolean: Animate automatically, true or false
      speed: 500,             // Integer: Speed of the transition, in milliseconds
      timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
      pager: false,           // Boolean: Show pager, true or false
      nav: true,              // Boolean: Show navigation, true or false
      random: false,          // Boolean: Randomize the order of the slides, true or false
      pause: false,           // Boolean: Pause on hover, true or false
      pauseControls: true,    // Boolean: Pause when hovering controls, true or false
      prevText: "Anterior &nbsp;&nbsp;&nbsp;&nbsp;",   // String: Text for the "previous" button
      nextText: "Siguiente",  // String: Text for the "next" button
      maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
      navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
      manualControls: "",     // Selector: Declare custom pager navigation
      namespace: "rslides",   // String: Change the default namespace used
      before: function(){},   // Function: Before callback
      after: function(){}     // Function: After callback
    });






    function activarMenu(value)
    {
        var link = new Array();
        link[1]  = "banner";
        link[2]  = "quienes-somos";
        link[3]  = "metodo-krasiba";
        link[4]  = "coaching";
        link[5]  = "instalaciones";
        link[6]  = "album";
        link[7]  = "eventos";
        link[8]  = "testimonios";
        link[9]  = "tour";
        link[10] = "ubicacion";
        link[11] = "contacto";

        if (link[value] == 'metodo-krasiba') {
          if ( nivelDePagina == 'index-in') {
            window.location.href = 'metodo-krasiba.html';
          }
          else if ( nivelDePagina == 'index-out') {
            window.location.href = '../../metodo-krasiba.html';
          }
          else if ( nivelDePagina == '1') {
              window.location.href = '../metodo-krasiba.html';
          }
          else if ( nivelDePagina == '2') {
              window.location.href = '../../metodo-krasiba.html';
          }
          else if ( nivelDePagina == '3') {
              window.location.href = '../../../metodo-krasiba.html';
          }
          else {
            alert('ERROR! Dentro de metodo-krasiba.html.\nNo se puede determinar el nivel de la página actual dentro del árbol de carpetas.');
          }
        }
        else if (link[value] != 'metodo-krasiba') {
          if (nivelDePagina == 'index-in') {
            window.location.href = "#"+link[value];
          }
          else if (nivelDePagina == 'index-out') {
            window.location.href = "../../index.html#"+link[value];
          }
          else if (nivelDePagina == '1') {
            window.location.href = "../index.html#"+link[value];
          }
          else if (nivelDePagina == '2') {
            window.location.href = "../../index.html#"+link[value];
          }
          else if (nivelDePagina == '3') {
            window.location.href = "../../../index.html#"+link[value];
          }
          else {
            alert('ERROR! Fuera de metodo-krasiba.html.\nNo se puede determinar el nivel de la página actual dentro del árbol de carpetas.');
          }
        }
        else {
          alert('ERROR! Opción no reconocida.\nNo se puede determinar el nivel de la página actual dentro del árbol de carpetas.');
        }
    }


    var browser = $.browser.browser();
    // if (browser == 'Mozilla') {   Checar aquí, porque la librería JQBrowser llama Mozilla a Chrome
    // if (browser == 'Mozilla') {
    if (browser == 'Internet Explorer') {
      $(".spanCajaEmergente" ).hide("slow");
      $("#clickAqui").css("color","white");
      $("#submitFormaNewsletter").css("position","relative");
      $("#submitFormaNewsletter").css("top","-9px");
      $("#linkAP01, #linkAP02").css("font-size","11px");
    }
  
</script>




