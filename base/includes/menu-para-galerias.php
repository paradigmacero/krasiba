<!-- START HEADER -->
<header id="menu">
    <!-- START LOGO -->
<!-- 
    <div class="logo-wrapper">
        <a id="logo" href="index-slider.html"><img src="baase/img/logos/krasiba.png" alt="FLATO" /></a>
    </div> -->

    <!-- ....................  MENU  .................... -->
    <!-- ....................  MENU  .................... -->
    <!-- ....................  MENU  .................... -->

    <!-- START NAVIGATION -->
    <ul id="main-navigation">
   <!-- <li id="menu-li-1"><a class="active" href="#banner">Inicio</a></li> -->
        <li id="menu-li-1">
            <a class="active" href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#banner';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#banner';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#banner';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#banner';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#banner';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Inicio</a>
        </li>




        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#quienes-somos';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#quienes-somos';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#quienes-somos';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#quienes-somos';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#quienes-somos';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Quiénes Somos</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo 'metodo-krasiba.html';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo 'metodo-krasiba.html';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../metodo-krasiba.html';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../metodo-krasiba.html';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../metodo-krasiba.html';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Método Krasiba</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#coaching';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#coaching';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#coaching';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#coaching';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#coaching';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Coaching y Terapia con Caballos</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#instalaciones';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#instalaciones';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#instalaciones';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#instalaciones';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#instalaciones';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Instalaciones</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#album';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#album';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#album';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#album';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#album';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Álbum</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#eventos';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#eventos';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#eventos';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#eventos';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#eventos';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Eventos Especiales</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#testimonios';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#testimonios';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#testimonios';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#testimonios';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#testimonios';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Testimonios</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#tour';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#tour';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#tour';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#tour';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#tour';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Tour</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#ubicacion';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#ubicacion';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#ubicacion';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#ubicacion';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#ubicacion';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Ubicación</a>
        </li>



            
        <li>
            <a href="
            <?php
                if($nivelDePagina === 'index-in') {
                    echo '#contacto';
                }
                elseif ($nivelDePagina === 'index-out') {
                    echo '../../index.html#contacto';
                }
                elseif ($nivelDePagina === '1') {
                    echo '../index.html#contacto';
                }
                elseif ($nivelDePagina === '2') {
                    echo '../../index.html#contacto';
                }
                elseif ($nivelDePagina === '3') {
                    echo '../../../index.html#contacto';
                }
                else {
                    echo 'ERROR! No se puede determinar el nivel de la página actual dentro del árbol de carpetas.';
                }
            ?>
            ">Contacto</a>
        </li>
    </ul><!-- END NAVIGATION -->





    <!-- ....................  MENU SELECT  .................... -->
    <!-- ....................  MENU SELECT  .................... -->
    <!-- ....................  MENU SELECT  .................... -->
    <select id="menuSelect" onchange="activarMenu(this.value)">
        <option>:: Menú ::</option>
        <option value="1"> Inicio</option>
        <option value="2"> Quiénes somos</option>
        <option value="3"> Método Krasiba</option>
        <option value="4"> Coaching y Terapia con caballos</option>
        <option value="5"> Instalaciones</option>
        <option value="6"> Álbum</option>
        <option value="7"> Eventos Especiales</option>
        <option value="8"> Testimonios</option>
        <option value="9"> Tour</option>
        <option value="10">Ubicación</option>
        <option value="11">Contacto</option>
    </select>





<!-- 
    <div class="social-links">
        <a href=""><i class="fa fa-twitter"></i></a>
        <a href=""><i class="fa fa-facebook"></i></a>
        <a href=""><i class="fa fa-linkedin"></i></a>
    </div> -->
	
    <a id="open-nav"><i class="fa fa-bars"></i></a>
</header><!-- CLOSE HEADER -->
