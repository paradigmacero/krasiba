	<!-- START BANNER SECTION -->
    <section id="banner" class="section">
		
		<!-- START ROW -->
        <div class="banner-container">
            <img src="base/img/logos/krasiba.png" alt="">
            <span class="big-text">
                CAMPO KRASIBA
            </span>
            <span class="small-text">
                UN LUGAR DONDE EL <span class="bold">"NO PUEDO"<span class="color"> NO </span></span> EXISTE
            </span>
            <div class="button-wrapper">
                <a class="button scrollto" href="#contacto">Contáctanos</a>
                <!-- <a class="button border">Purchase Now</a> -->
            </div>
        </div><!-- END ROW -->
		
		<!-- START SCROLL DOWN BUTTON -->
        <div class="scroll-down">
            <a id="scrollto" class="scrollto" href="#contacto"></a>
        </div><!-- END SCROLL DOWN BUTTON -->
		
		
		<a class="maximage-next"></a>
		<a class="maximage-prev"></a>
		
		<!-- START MAXIMAGE SLIDER -->
        <div id="maximage-slider">
            <div>
                <img src="base/img/secciones/header/01.jpg" alt="" />
            </div>

<!--             <div>
                <img src="base/img/secciones/header/01.jpg" alt="" />
            </div>
            <div>
                <img src="base/img/secciones/header/01.jpg" alt="" />
            </div> -->
        </div><!-- END MAXIMAGE SLIDER -->
		
    </section><!-- END BANNER SECTION -->