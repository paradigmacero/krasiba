    <!-- START BLOG SECTION -->
    <section id="contacto">
        
            <!-- START WRAPPER -->
        <div class="headline-wrapper contact-headline">
            <!-- START ROW -->
            <div class="row">
                <h1 class="underline">
                    Contacto
                </h1>
                <p class="undertitle">
                    Envíanos un mensaje a <a id="correoKrasiba" href="mailto:campokrasiba@gmail.com">campokrasiba@gmail.com</a>
                    <br>
                    o llámanos a los teléfonos: +52 (55) 5511 1554 • 5208 9691 • 5208 9425
                </p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->    
            
<!--             
        <div class="row">
        </div>
 -->

        <!-- START PARALLAX CONTENT -->
        <div class="parallax-content parallax-contact">
            
            <!-- START ROW -->
            <div class="row">
                <!-- START CONTACT FORM -->
                <div class="contact-form">

                    <form id='formContacto' action="process.php" method="post" onsubmit="return validarContacto()">
                        Nombre*
                        <br>
                        <input id="nombre" name="nombre" type="text" class="text cleartext" onkeypress="return mayusMinus(event,this)" />
                        Email*
                        <br>
                        <input id="email" name="email" type="text" class="text cleartext" />
                        Teléfono*
                        <br>
                        <input id="telefono" name="telefono" type="text" class="text cleartext" onkeypress="return soloNumeros(event)" />
                        Empresa*
                        <br>
                        <input id="empresa" name="empresa" type="text" class="text cleartext" />
                        Comentario*
                        <br>
                        <textarea id="comentario" name="comentario"></textarea>
                        <input type="submit" class="submit" value="Enviar" id="submitFormaContacto" />
                        <input type="text" name="address">
                        <input type="hidden" name="forma" value="formaContacto">
                    </form>

                </div><!-- END CONTACT FORM -->
            </div><!-- END ROW -->
            
        </div><!-- END PARALLAX CONTENT -->        
                   
    </section><!-- START BLOG SECTION -->


            