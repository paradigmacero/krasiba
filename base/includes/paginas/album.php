	<!-- START SERVICE SECTION -->
	<section id="album">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Galería Fotográfica
				</h1>
                <p class="undertitle">
                    <a id="clickAqui" href="galeria.html">
                        Da click aquí para visitar nuestro álbum
                    </a>
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->	

	</section>	<!-- END SERVICE SECTION -->
