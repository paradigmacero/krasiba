

	<!-- START BLOG SECTION  -->
    <section id="quienes-somos">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Quiénes Somos
				</h1>
                <p class="undertitle">
					Campo Krasiba fue fundado en el año 2000. Somos un grupo de profesionales de distintas disciplinas, con amplia experiencia en el mundo corporativo y académico.
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->

		
		<!-- START WRAPPER -->
        <div id="blog-wrapper">
            <div class="close">
                <a id="close-blog-wrapper"></a>
            </div>
            <div id="blog-loader"><img src="images/layout/color_loader.gif" alt="" /></div>

			<div id="load-blog-content">
				<!-- Blog Details load here -->
			</div>
        </div><!-- END WRAPPER -->
		
			
		<!-- START ROW -->
        <div class="row">
			<!-- START BLOG GRID -->
            <ul id="blog-preview">
				<!-- START BLOG ARTICLE -->
                <li id="qs-li-01">
                    <div class="col span_4">
                    </div>                    
                    <div class="col span_8">
                        <span class="detail">
                            Estamos comprometidos en la transformación de nuestro país y para ello elegimos aplicar nuestra experiencia nacional e internacional en los ámbitos financiero, académico, diplomático, desarrollo humano, coaching, alta dirección de empresas, <!-- SEO:  --> capacitacion cmpresarial <!-- /SEO:  --> y servicio público en la misión de construir equipos de alto desempeño.
                            <br>
                            <br>
                            Lo que nos hace diferentes es que todos somos amantes y practicantes de deportes extremos y de aventura: Montañismo, escalada en roca, espeleismo, buceo, ala delta, espeleobuceo, paracaidismo, cañonismo, treaking, rafting, etc, llevamos al mundo corporativo los valores que rigen al mundo de la montaña, trabajando con impecabilidad en cada paso que damos, creando espacios donde las personas se encuentran con su yo interno y permiten crear armonía en el ambiente laboral (lo que se denomina team building Mexico). Estamos preparados para una verdadera capacitacion vivencial.
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->
					
				<!-- START BLOG ARTICLE -->
                <li id="qs-li-02">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
							Misión
						</h4>
                        <span class="detail">
                            Buscamos contribuir a la construcción de una sociedad más sana, eficiente y justa mediante el desarrollo integral del individuo, posibilitándolo para encontrar su verdadero sentido de identidad, a través del incremento de la autoestima, descubrimiento de potencialidades, superación de miedos, generación de confianza, comunicación profunda con el yo y con el otro, así como su identificación como parte de la naturaleza. Este individuo es la base de organizaciones sanas, eficientes, responsables y poderosas. Todo esto mediante una capacitación vivencial de extraordinaria fuerza.
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->
					
				<!-- START BLOG ARTICLE -->
                <li id="qs-li-03">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
							Visión
						</h4>
                        <span class="detail">
                            La visión de Campo Krasiba es la del águila, que contempla al mundo desde su vuelo mas allá de las nubes, donde todo es posible y los logros dependen de las propias alas.
                            <br>
                            <br>
                            Soñamos con un mundo donde el individuo sepa ejercer su libertad con plenitud y responsabilidad, donde de rienda suelta a su imaginación.
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

            </ul><!-- END BLOG GRID -->
				
			<!-- START LOAD MORE BUTTON -->
<!-- 
            <div id="blog-load-more" class="blog-button">
                <a class="button border">
                    Load More
                    <img src="images/layout/color_loader.gif" alt="Loading" />
                </a>
            </div> -->
				
        </div><!-- END ROW -->
			
			
		<!-- START PARALLAX CONTENT -->
<!--         
        <div class="parallax-content parallax-newsletter">
            <div class="row">
                <h3>Do you like it?</h3>
                <p class="max-width">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                </p>
                <a class="button border">Purchase Now</a>
            </div>
        </div> -->
		
    </section><!-- START BLOG SECTION -->

