	<!-- START SERVICE SECTION -->
	<section id="instalaciones">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Instalaciones
				</h1>
                <p class="undertitle">
					Campo Krasiba cuenta con un campus de 18,000 m2, alojado en un bosque de 300 hectáreas, que nos permite desplegar nuestros programas en un ambiente de relajación, concentración y contacto con la naturaleza, libre de las tensiones urbanas y de la atmósfera de trabajo.
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->


		<!-- START ROW -->
        <div class="row gutters">
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START ICON BOX -->
                <div class="icon-box">
                    <img src="base/img/secciones/instalaciones/01.jpg" alt="" />
                    <h4>Salón Panorámico</h4>
                    <p>
                        Campo Krasiba ofrece la posibilidad de celebrar cualquier tipo de reunión enmarcada por la magia del bosque, contando con todas las facilidades didácticas y tecnológicas.
                        <br>
                        <br>
                        &bull;Con capacidad para 200 personas dependiendo el formato.<br>
                        &bull;Paredes de proyección de 60,15 y 12 m2.<br>
                        &bull;Pared pizarrón de 4 m2.<br>
                        &bull;Fogón.<br>
                        &bull;Acceso a internet.<br>
                        &bull;Puertas de cristal abatibles.<br>
                        &bull;Cocina integrada.<br>
                        &bull;Equipo de sonido y proyección.<br>
                        &bull;Mobiliario para eventos.<br>
                        &bull;Ventanal de 8 m. de altura y vista espectacular hacia el bosque de coníferas.<br>
                        &bull;200 m2 con piso de madera de Teka.<br>
                        &bull;Explanada contigua que permite duplicar la superficie de trabajo.<br>
                        &bull;Posibilidad de eventos mixtos, aprovechando la infraestructura de Campo Krasiba.<br>
                        &bull;Áreas de estacionamiento dentro de las instalaciones.<br>
                        &bull;Servicio de alimentos con la tradicional calidad de la cocina Krasiba.<br>
                        &bull;Total privacidad.                        <br>
                    </p>
                </div><!-- END ICON BOX -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START ICON BOX -->
                <div class="icon-box">
                    <img src="base/img/secciones/instalaciones/02.jpg" alt="" />
                    <h4>Campus de 18,000 M²</h4>
                    <p>
                        Campo Krasiba cuenta con un campus de 18,000 m2, alojado en un bosque de 300 hectáreas, que nos permite desplegar nuestros programas en un ambiente de relajación, concentración y contacto con la naturaleza, libre de las tensiones urbanas y de la atmósfera de trabajo.
                        <br>
                        <br>
                        &bull;Salón de usos múltiples (comedor) con capacidad hasta 120 personas dependiendo el formato<br>
                        &bull;Salón panorámico con capacidad hasta 200 personas dependiendo el formato.<br>
                        &bull;2 aulas ejecutivas con cámara de Gesell.<br>
                        &bull;Ágora y patio ingles con asador y jardín vertical para cursos y talleres al aire libre.<br>
                        &bull;Caballerizas y pista ecuestre para la impartición de clínicas de Coaching asistido con caballos.<br>
                        &bull;Pista de comandos (réplica a escala de una pista de entrenamiento militar).<br>
                        &bull;Canopy (puentes colgantes en las copas de los árboles a 15 metros de altura).<br>
                        &bull;Tirolesa de 60 metros de longitud.<br>
                        &bull;Laberinto subterráneo.<br>
                        &bull;Peñas naturales habilitadas para la práctica de actividades de alta montaña, rappel y escalada en roca, con un desnivel de 300 mts. sobre el valle.<br>
                        &bull;Hospedaje en cabañas ecológicas.<br>
                        &bull;Baños y regaderas con agua caliente.<br>
                        &bull;Área de fogata.<br>
                        &bull;Cancha de fútbol con pasto natural.<br>
                        &bull;Servicio de alimentos.<br>
                        &bull;Acceso a internet                        <br>
                    </p>
                </div><!-- END ICON BOX -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START ICON BOX -->
                <div class="icon-box">
                    <img src="base/img/secciones/instalaciones/03.jpg" alt="" />
                    <h4>Aulas Ejecutivas</h4>
                    <p>
                        2 aulas ejecutivas equipadas con capacidad hasta 35 personas cada una dependiendo el formato.
                        <br>
                        <br>
                        &bull;Cámara de Gesell<br>
                        &bull;Sala V.I.P. acondicionada para recesos.<br>
                        &bull;Ágora y patio ingles con asador y jardín vertical para cursos y talleres al aire libre.<br>
                        &bull;Acceso a internet.<br>
                        &bull;Equipo de sonido y proyección.<br>
                        &bull;Áreas de estacionamiento dentro de las instalaciones.<br>
                        &bull;Servicio de alimentos con la tradicional calidad de la cocina Krasiba.<br>
                        &bull;Total privacidad                        <br>
                    </p>
                </div><!-- END ICON BOX -->
            </div><!-- END COLUMN 4/12 -->
        </div><!-- END ROW -->
			

	</section>	<!-- END SERVICE SECTION -->
