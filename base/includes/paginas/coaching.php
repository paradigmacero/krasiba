    <!-- START BLOG SECTION -->
    <section id="coaching">
        
        <!-- START WRAPPER -->
        <div class="headline-wrapper">
            <!-- START ROW -->
            <div class="row">
                <h1 class="underline">
                    Coaching y Terapia con Caballos
                </h1>
                <p class="undertitle">
                    <!-- xxxxxs -->
                </p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->

        
        <!-- START WRAPPER -->
        <div id="blog-wrapper">
            <div class="close">
                <a id="close-blog-wrapper"></a>
            </div>
            <div id="blog-loader"><img src="images/layout/color_loader.gif" alt="" /></div>

            <div id="load-blog-content">
                <!-- Blog Details load here -->
            </div>
        </div><!-- END WRAPPER -->
        
            
        <!-- START ROW -->
        <div class="row">
            <!-- START BLOG GRID -->
            <ul id="blog-preview">
                <!-- START BLOG ARTICLE -->
                <li id="ctc-li-01">
                    <div class="col span_4">
                    </div>                    
                    <div class="col span_8">
                        <span class="detail">
                            La manada equina nos enseña su forma de trabajar en equipo, de comunicarse, de relacionarse entre sus miembros y con el hombre.
                            <br>
                            <br>
                            El caballo no miente; es directo y transparente; desde su lugar en el mundo, donde se concibe como “presa”, permite al hombre acercarse, entablar contacto, construir puentes de confianza y credibilidad.
                            <br>
                            <br>
                            El caballo es un espejo, honesto, congruente y transparente frente a las actitudes del grupo son sumamente sensibles e imponentes, si no existe cohesión en el equipo se verá reflejado en la conducta de la manada equina.
                            <br>
                            <br>
                            Se podrá observar y reflejar cuál es la problemática del grupo. Por medio del trabajo con ellos podremos llegar al fondo, a la esencia congruente del grupo de personas.
                            <br>
                            <br>
                            Las terapias en caballos son una lección de vida; nos enseñan desde su esencia, las reglas básicas de la comunicación, la confianza, la empatía, el respeto, el liderazgo y la convivencia.
                            <br>
                            <br>
                            Descubre lo que el coaching con caballos puede hacer por ti y por tu equipo de trabajo.
                            <br>
                            <br>
                            Programa certificado por EAGALA (Equine Assisted for Growth and Learning). En coaching con caballos mexico está a la vanguardia.
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->
            </ul><!-- END BLOG GRID -->
                
            <!-- START LOAD MORE BUTTON -->
<!-- 
            <div id="blog-load-more" class="blog-button">
                <a class="button border">
                    Load More
                    <img src="images/layout/color_loader.gif" alt="Loading" />
                </a>
            </div> -->
                
        </div><!-- END ROW -->
            
            
        <!-- START PARALLAX CONTENT -->
<!--         
        <div class="parallax-content parallax-newsletter">
            <div class="row">
                <h3>Do you like it?</h3>
                <p class="max-width">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                </p>
                <a class="button border">Purchase Now</a>
            </div>
        </div> -->
        
    </section><!-- START BLOG SECTION -->

