	<!-- START PRICING SECTION -->
    <section id="ubicacion">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Ubicación
				</h1>
                <p class="undertitle">
					Kilómetro 36.5 de la carretera México – Toluca
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->


		<!-- START ROW -->
        <div class="row gutters">
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table">
                    <div class="header">
                        <h3>
							A 25 minútos de
						</h3>
                        <span class="price">
							Santa Fe
						</span>
                    </div>
                    <div class="content">
                        Campo Krasiba se encuentra a 25 minutos de Santa Fe, en la Cañada de Alférez, zona de la Marquesa, Municipio de Lerma, Estado de México, kilómetro 36.5 de la carretera México – Toluca.
                    </div>
                    <div class="footer">
                        <!-- <a class="button border">Purchase Now</a> -->
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table best-price">
                    <div class="header">
                        <h3>
							<!-- Basic Edition -->
						</h3>
                        <span class="price">
							Mapa
						</span>
                    </div>
                    <div class="content">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d60241.33734349138!2d-99.409887!3d19.322179!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8e72e4fa5a8ecb06!2sCampo+Krasiba!5e0!3m2!1sen!2sus!4v1403102688886" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
                    </div>
                    <div class="footer">
                        <a href="base/descargas/Mapa-CK-Google-2014.pdf" target="_blank">Descargar mapa</a>
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table">
                    <div class="header">
                        <h3>
							En un bosque virgen
						</h3>
                        <span class="price">
							18,000 m²
						</span>
                    </div>
                    <div class="content">
                        Krasiba cuenta con un campus de 18,000 m2, alojado en un bosque virgen de 300 hectáreas, rodeado por riachuelos y manantiales, que nos permite desplegar nuestros programas en un ambiente de relajación, concentración y contacto con la naturaleza, libre de las tensiones urbanas y de la atmósfera de trabajo.
                    </div>
                    <div class="footer">
                        <!-- <a class="button border">Purchase Now</a> -->
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
        </div><!-- END ROW -->
		
		


    </section><!-- END PRICING SECTION -->


