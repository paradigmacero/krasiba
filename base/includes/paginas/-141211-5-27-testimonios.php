
	<!-- START PRICING SECTION -->
    <section id="testimonios">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Testimonios
				</h1>
                <p class="undertitle">
					<!-- xxxxxxx -->
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->



        <div class="row">
	        <div class="col span_4">
	            <ul class="rslidesTestimonios">
	              <li><img src="base/img/secciones/testimonios/01.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/02.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/03.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/04.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/05.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/06.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/07.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/08.jpg" alt=""></li>
	              <li><img src="base/img/secciones/testimonios/09.jpg" alt=""></li>
	            </ul>
	        </div>

	        <div id="testimoniosPrincipales" class="col span_8">
		        <div class="metodoCaja01x row">
		            <div class="metodoCaja03x row testimonios-alfa-2">
		              <!-- TEXTO -->


						<div class="testimonioWrapper">



		                    <div id="div1a" onclick="toogle(this,'div2a','div3a','div4a','div5a','div6a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign1" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Ramón Arroyo Ramos | <span class="testimonioName2">BBVA Bancomer</span>
		                        </div>
		                    </div>
		                    <div id="div1b" class="testimonioComentario">
		                        Con nuestro más grande agradecimiento y reconocimiento al equipo de Campo Krasiba y su líder Eduardo Fuentes.
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Ramón Arroyo Ramos<br />
		                            Director Corporativo de RH <br />
		                            BBVA Bancomer <br />
		                            22 de Octubre 2009
		                        </span>
		                    </div>



		                    <div id="div2a" onclick="toogle(this,'div1a','div3a','div4a','div5a','div6a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign2" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            MAC | <span class="testimonioName2">MAC</span>
		                        </div>
		                    </div>
		                    <div id="div2b" class="testimonioComentario">
		                        La verdad es que no se bien por dónde comenzar, ni como organizar mis ideas, lo único que les puedo escribir es que me voy plena, satisfecha, con mucha energía positiva y sólo les agradezco una y otra vez el haberme apoyado en esta aventura tan maravillosa.
		                        <br />
		                        <br />
		                        Dios los cuide y los bendiga 
		                        Agradecida hasta el infinito
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -MAC<br />
		                            13 de noviembre 2009
		                        </span>
		                    </div>







		                    <div id="div3a"onclick="toogle(this,'div1a','div2a','div4a','div5a','div6a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign3" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Mara Pérez | <span class="testimonioName2">BBVA Bancomer</span>
		                        </div>
		                    </div>
		                    <div id="div3b" class="testimonioComentario">
		                        Equipo Krasiba:<br /><br />
		                        Muchas gracias por su hospitalidad, por sus enseñanzas y por su sencillez Este evento nos cambiará la vida como personas y como equipo; es una gran contribución que ustedes hacen a las organizaciones. <br /><br />
		                        Sin duda que nos hacen ser mejores personas y eso no tiene precio, ni tiene forma de ser gratificado; simplemente es ENORME Muchas, muchas gracias
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Mara Pérez<br />
		                            Dirección Sureste Banca de Empresas y Gobierno<br />
		                            BBVA Bancomer <br />
		                            13 de noviembre 2009
		                        </span>
		                    </div>











		                    <div id="div4a" onclick="toogle(this,'div1a','div2a','div3a','div5a','div6a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign4" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Pilar | <span class="testimonioName2">Grupo AXO</span>
		                        </div>
		                    </div>
		                    <div id="div4b" class="testimonioComentario">
		                        -Estoy enamorada de Krasiba<br />
		                        Su gente y su espacio, me llenan de amor, ilusión, fe, fortaleza, armonía, unión, pasión y muchas virtudes más.<br />
		                        Gracias por darme la oportunidad de probar a la naturaleza y al paraíso
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Pilar<br />
		                            Gerente de Capacitación <br />
		                            Grupo AXO
		                        </span>
		                    </div>







		                    <div id="div5a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div6a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign5" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Luis Felipe Alva | <span class="testimonioName2">Sistemas Neumáticos de Envíos</span>
		                        </div>
		                    </div>
		                    <div id="div5b" class="testimonioComentario">
		                        Estimado Krasiba:<br /><br />
		                        Muchas gracias por todo su apoyo Estoy seguro que serán una pieza clave para lograr nuestros objetivos planteados para el 2010, 2011... etc.
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Luis Felipe Alva<br />
		                            Director General <br />
		                            Sistemas Neumáticos de Envíos S.A. de C.V. <br />
		                            29 de noviembre 2009
		                        </span>
		                    </div>







		                    <div id="div6a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div7a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign6" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Arturo Villanueva | <span class="testimonioName2">BBVA Bancomer</span>
		                        </div>
		                    </div>
		                    <div id="div6b" class="testimonioComentario">
		                        Estimado equipo de Krasiba: <br /><br />
		                        En BBVA Bancomer estamos trabajando para transformarnos en un proceso punta apunta, desde nuestros clientes, foco de nuestra actividad, hasta el último rincón de las áreas de apoyo. RH, es una de ellas, clave en la estrategia y fundamental en la construcción del “Mejor Lugar Para Trabajar”; y hemos encontrado en KRASIBA, “El Mejor Lugar Para Aprender y Desarrollarnos” Gracias por su profesionalismo y por brindarnos su casa, como una extensión de nosotros.<br /><br />
		                        Felicidades y Adelante
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Arturo Villanueva<br />
		                            Director de Capacitación <br />
		                            BBVA Bancomer <br />
		                            21 de Abril de 2010
		                        </span>
		                    </div>







		                    <div id="div7a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div6a','div8a','div9a','div10a','div11a');">
		                        <div id="testimonioSign7" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Ramón Arroyo Ramos | <span class="testimonioName2">BBVA Bancomer</span>
		                        </div>
		                    </div>
		                    <div id="div7b" class="testimonioComentario">
		                        Para mi amigo Eduardo Fuentes y su maravilloso equipo: <br /><br />
		                        Muchas gracias por permitirnos aprender con ustedes; muchas gracias por su guía, pero sobre todo muchas gracias por todo lo bien que nos han hecho sentir en estas jornadas de integración que ya forman parte de nuestras experiencias de vida. <br /><br />
		                        Sinceramente
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Ramón Arroyo Ramos<br />
		                            Director Corporativo de RH <br />
		                            BBVA Bancomer <br />
		                            21 de Abril de 2010
		                        </span>
		                    </div>







		                    <div id="div8a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div6a','div7a','div9a','div10a','div11a');">
		                        <div id="testimonioSign8" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Ricardo Torres Nava | <span class="testimonioName2">Montañista mexicano</span>
		                        </div>
		                    </div>
		                    <div id="div8b" class="testimonioComentario">
		                        Estimado Krasiba: <br /><br />
		                        Con mi agradecimiento, cariño y fraternidad, para mis hermanos y familia de Campo Krasiba. <br /><br />
		                        Que la luz e inmensidad de la naturaleza iluminen sus senderos, para seguir superando los “Everests Cotidianos”; a veces, los más difíciles de escalar.
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Ricardo Torres Nava<br />
		                            Montañista mexicano; primer latinoamericano en lograr la cumbre del Everest y poseedor del Gran Slam de montañismo (Cumbre más alta de todos los continentes). <br />
		                            Abril de 2010
		                        </span>
		                    </div>







		                    <div id="div9a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div6a','div7a','div8a','div10a','div11a');">
		                        <div id="testimonioSign9" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Radamés Vargas Ramirez | <span class="testimonioName2">SAI a Gasolineras</span>
		                        </div>
		                    </div>
		                    <div id="div9b" class="testimonioComentario">
		                        Estimado Lalo:<br /><br />
		                        Mil gracias por el apoyo brindado; te confieso que el primer día tuve pánico de que se desvirtuara el objetivo, sin embargo, este se cumplió con creces. Ahora nuestro reto es continuar con lo aprendido<br /><br />
		                        Nuevamente mil gracias. <br />
		                        Con cariño y aprecio
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Radamés Vargas Ramirez<br />
		                            Sistemas Administrativos Integrales a Gasolineras <br />
		                            25 de Abril de 2010.
		                        </span>
		                    </div>









		                    <div id="div10a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div6a','div7a','div8a','div9a','div11a');">
		                        <div id="testimonioSign10" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Edgar Karam | <span class="testimonioName2">BBVA Bancomer</span>
		                        </div>
		                    </div>
		                    <div id="div10b" class="testimonioComentario">
		                        Querido Lalo:<br /><br />
		                        La grandeza de tu alma me llena de vida, de actitud y de positivismo. Lo que tu haces es grandioso y nos deja con la reflexión de vida.<br /><br />
		                        Tú, el equipo que tienes y todos los apoyos que nos han brindado, son únicos para el bien estar de la humanidad<br /><br />
		                        Muchas gracias<br />
		                        Muchas bendiciones<br />
		                        Por siempre
		                        <br />
		                        <br />
		                        <span class="testAutor">
		                            -Edgar Karam<br />
		                            Director de la Red PYME <br />
		                            BBVA Bancomer <br />
		                            25 de Junio de 2010
		                        </span>
		                    </div>







		                    <div id="div11a" onclick="toogle(this,'div1a','div2a','div3a','div4a','div5a','div6a','div7a','div8a','div9a','div10a');">
		                        <div id="testimonioSign11" class="testimonioTitleA">
		                            +
		                        </div>
		                        <div class="testimonioTitleB">
		                            Mónica Moreno Rojo | <span class="testimonioName2">Pepsico</span>
		                        </div>
		                    </div>
		                     <div id="div11b" class="testimonioComentario" style="height: 215px; padding-top: 10px; border-bottom: 1px solid white;">
		                        Quería agradecerte muchísimo por todas las actividades el martes, la verdad fue bueníiiiisiiima experiencia y nos ayuda a crecer y ser mejores. Por favor haz extensivo este agradecimiento a todo tu equipo, a la gente de la cocina que uuufff que rico comimos, a los Aldos, en fin, a todos!!! Que estés muy bien y espero que sigamos en contacto.<br><br>
		                        Saludos!!!!<br><br>
		                        Gracias de nuevo! ;0)
		                        <br>
		                        <br>
		                        <span class="testAutor">
		                            -Mónica Moreno Rojo<br>
		                            Pepsico
		                        </span>
		                    </div>


		                </div>		                


		            </div>

		        </div>
	        </div>



<!-- TESTIMONIOS ALTERNATIVOS (responden mucho mejor a la parte responsiva en dimensiones muy pequeñas) -->
<!-- TESTIMONIOS ALTERNATIVOS (responden mucho mejor a la parte responsiva en dimensiones muy pequeñas) -->
<!-- TESTIMONIOS ALTERNATIVOS (responden mucho mejor a la parte responsiva en dimensiones muy pequeñas) -->
			<div id="testimoniosAlternativos" class="col span_8">
	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Ramón Arroyo Ramos | <span class="testimonioName2">BBVA Bancomer</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                        Con nuestro más grande agradecimiento y reconocimiento al equipo de Campo Krasiba y su líder Eduardo Fuentes.
	                        <br />
	                        <br />
	                        <span class="testAutor">
	                            -Ramón Arroyo Ramos<br />
	                            Director Corporativo de RH <br />
	                            BBVA Bancomer <br />
	                            22 de Octubre 2009
	                        </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            MAC | <span class="testimonioName2">MAC</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            La verdad es que no se bien por dónde comenzar, ni como organizar mis ideas, lo único que les puedo escribir es que me voy plena, satisfecha, con mucha energía positiva y sólo les agradezco una y otra vez el haberme apoyado en esta aventura tan maravillosa.
	                            <br />
	                            <br />
	                            Dios los cuide y los bendiga 
	                            Agradecida hasta el infinito
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -MAC<br />
	                                13 de noviembre 2009
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Mara Pérez | <span class="testimonioName2">BBVA Bancomer</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Equipo Krasiba:<br /><br />
	                            Muchas gracias por su hospitalidad, por sus enseñanzas y por su sencillez Este evento nos cambiará la vida como personas y como equipo; es una gran contribución que ustedes hacen a las organizaciones. <br /><br />
	                            Sin duda que nos hacen ser mejores personas y eso no tiene precio, ni tiene forma de ser gratificado; simplemente es ENORME Muchas, muchas gracias
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Mara Pérez<br />
	                                Dirección Sureste Banca de Empresas y Gobierno<br />
	                                BBVA Bancomer <br />
	                                13 de noviembre 2009
	                            </span>

	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Dalia<span class="testimonioName2"></span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Una experiencia mágica. Gracias por hacer de este día un Punto de Partida<br /><br />
	                            Grandiosa es la gente que brinda rayos de luz a la vida de otros………
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Dalia<br />
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Lizette<span class="testimonioName2"></span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Hola, solo una cosa tienen mal; no aprendieron a contar bien; no somos un equipo de, 19 personas, somos uno de 24. Gracias Rojo, Edgar, Alejandro, Pepe y Eduardo.<br />
	                            Hasta muy pronto<br />
	                            Besos
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Lizette<br />
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Pilar | <span class="testimonioName2">Grupo AXO</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            -Estoy enamorada de Krasiba<br />
	                            Su gente y su espacio, me llenan de amor, ilusión, fe, fortaleza, armonía, unión, pasión y muchas virtudes más.<br />
	                            Gracias por darme la oportunidad de probar a la naturaleza y al paraíso
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Pilar<br />
	                                Gerente de Capacitación <br />
	                                Grupo AXO
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Luis Felipe Alva | <span class="testimonioName2">Sistemas Neumáticos de Envíos</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Estimado Krasiba:<br /><br />
	                            Muchas gracias por todo su apoyo Estoy seguro que serán una pieza clave para lograr nuestros objetivos planteados para el 2010, 2011... etc.
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Luis Felipe Alva<br />
	                                Director General <br />
	                                Sistemas Neumáticos de Envíos S.A. de C.V. <br />
	                                29 de noviembre 2009
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Arturo Villanueva | <span class="testimonioName2">BBVA Bancomer</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Estimado equipo de Krasiba: <br /><br />
	                            En BBVA Bancomer estamos trabajando para transformarnos en un proceso punta apunta, desde nuestros clientes, foco de nuestra actividad, hasta el último rincón de las áreas de apoyo. RH, es una de ellas, clave en la estrategia y fundamental en la construcción del “Mejor Lugar Para Trabajar”; y hemos encontrado en KRASIBA, “El Mejor Lugar Para Aprender y Desarrollarnos” Gracias por su profesionalismo y por brindarnos su casa, como una extensión de nosotros.<br /><br />
	                            Felicidades y Adelante
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Arturo Villanueva<br />
	                                Director de Capacitación <br />
	                                BBVA Bancomer <br />
	                                21 de Abril de 2010
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Ramón Arroyo Ramos | <span class="testimonioName2">BBVA Bancomer</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Para mi amigo Eduardo Fuentes y su maravilloso equipo: <br /><br />
	                            Muchas gracias por permitirnos aprender con ustedes; muchas gracias por su guía, pero sobre todo muchas gracias por todo lo bien que nos han hecho sentir en estas jornadas de integración que ya forman parte de nuestras experiencias de vida. <br /><br />
	                            Sinceramente
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Ramón Arroyo Ramos<br />
	                                Director Corporativo de RH <br />
	                                BBVA Bancomer <br />
	                                21 de Abril de 2010
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Ricardo Torres Nava | <span class="testimonioName2">Montañista mexicano</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Estimado Krasiba: <br /><br />
	                            Con mi agradecimiento, cariño y fraternidad, para mis hermanos y familia de Campo Krasiba. <br /><br />
	                            Que la luz e inmensidad de la naturaleza iluminen sus senderos, para seguir superando los “Everests Cotidianos”; a veces, los más difíciles de escalar.
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Ricardo Torres Nava<br />
	                                Montañista mexicano; primer latinoamericano en lograr la cumbre del Everest y poseedor del Gran Slam de montañismo (Cumbre más alta de todos los continentes). <br />
	                                Abril de 2010
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Radamés Vargas Ramirez | <span class="testimonioName2">SAI a Gasolineras</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Estimado Lalo:<br /><br />
	                            Mil gracias por el apoyo brindado; te confieso que el primer día tuve pánico de que se desvirtuara el objetivo, sin embargo, este se cumplió con creces. Ahora nuestro reto es continuar con lo aprendido<br /><br />
	                            Nuevamente mil gracias. <br />
	                            Con cariño y aprecio
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Radamés Vargas Ramirez<br />
	                                Sistemas Administrativos Integrales a Gasolineras <br />
	                                25 de Abril de 2010.
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Edgar Karam | <span class="testimonioName2">BBVA Bancomer</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Querido Lalo:<br /><br />
	                            La grandeza de tu alma me llena de vida, de actitud y de positivismo. Lo que tu haces es grandioso y nos deja con la reflexión de vida.<br /><br />
	                            Tú, el equipo que tienes y todos los apoyos que nos han brindado, son únicos para el bien estar de la humanidad<br /><br />
	                            Muchas gracias<br />
	                            Muchas bendiciones<br />
	                            Por siempre
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Edgar Karam<br />
	                                Director de la Red PYME <br />
	                                BBVA Bancomer <br />
	                                25 de Junio de 2010
	                            </span>
	                    </div>
	                </div>
	            </div>


	            <div class="toggle-box">
	                <div class="toggle">
	                    <a class="header">
	                        <span class="icon plus">+</span>
	                        <span class="icon minus">-</span>
	                        <span class="text">
	                            Mónica Moreno Rojo | <span class="testimonioName2">Pepsico</span>
	                        </span>
	                    </a>
	                    <div class="content" style="">
	                            Quería agradecerte muchísimo por todas las actividades el martes, la verdad fue bueníiiiisiiima experiencia y nos ayuda a crecer y ser mejores. Por favor haz extensivo este agradecimiento a todo tu equipo, a la gente de la cocina que uuufff que rico comimos, a los Aldos, en fin, a todos!!! Que estés muy bien y espero que sigamos en contacto.<br /><br />
	                            Saludos!!!!<br /><br />
	                            Gracias de nuevo! ;0)
	                            <br />
	                            <br />
	                            <span class="testAutor">
	                                -Mónica Moreno Rojo<br />
	                                Pepsico
	                            </span>

	                    </div>
	                </div>
	            </div>
	        </div>


        </div>



		
		
    </section><!-- END PRICING SECTION -->

    <script type="text/javascript" src="scripts/scripts-d.js"></script>