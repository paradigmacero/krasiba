
	<!-- START PRICING SECTION -->
    <section id="eventos">
		
		<!-- START WRAPPER -->
        <div class="headline-wrapper">
			<!-- START ROW -->
            <div class="row">
                <h1 class="underline">
					Eventos Especiales
				</h1>
                <p class="undertitle">
					En Campo Krasiba llevamos a cabo programas con distintos objetivos:
				</p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->


		<!-- START ROW -->
        <div class="row gutters">
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table">
                    <div class="header">
                        <h3>
							Eventos Foráneos
						</h3>
                        <span class="price">
							<!-- 29 $ / month -->
						</span>
                    </div>
                    <div class="content">
                        Dirigido a empresas que no se encuentran en el área metropolitana o requieren su evento en otro lugar del país, llevamos los conocimientos, experiencia y logística al lugar adecuado.
                    </div>
                    <div class="footer">
                        <img src="base/img/secciones/eventos/01.jpg">
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table">
                    <div class="header">
                        <h3>
							Renta de Instalaciones
						</h3>
                        <span class="price">
							<!-- 39 $ / month -->
						</span>
                    </div>
                    <div class="content">
                        ¿Tienes juntas ó sesiones de trabajo? es el lugar ideal para salir de la rutina y del estrés de la ciudad.
                    </div>
                    <div class="footer">
                        <img src="base/img/secciones/eventos/02.jpg">
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
				
			<!-- START COLUMN 4/12 -->
            <div class="col span_4">
				<!-- START PRICING TABLE -->
                <div class="pricing-table">
                    <div class="header">
                        <h3>
							Festejo de Fin de Año
						</h3>
                        <span class="price">
							<!-- 49 $ / month -->
						</span>
                    </div>
                    <div class="content">
                        Damos un giro a las comidas típicas de fin de año, creando un evento diferente donde llevamos al mundo corporativo a vivir al máximo su cierre anual. Te apoyamos con la logística del evento, alimentos, dinámicas acorde al objetivo.
                    </div>
                    <div class="footer">
                        <img src="base/img/secciones/eventos/03.jpg">
                    </div>
                </div><!-- END PRICING TABLE -->
            </div><!-- END COLUMN 4/12 -->
        </div><!-- END ROW -->

    </section><!-- END PRICING SECTION -->

