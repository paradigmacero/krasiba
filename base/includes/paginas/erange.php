    <!-- START BLOG SECTION -->
    <section id="erange">
        

        <!-- START WRAPPER -->
        <div class="row">
            <div id="erangeSquaresContainer">
                <div class="erangeSquares">
                    <div class="erangeSquareWrapper">
                        <div class="erangeSquare1">
                            <a href="http://blog.campokrasiba.com.mx/" target="_blank" class="linkBackground"></a>
                        </div>
                        <div class="erangeCajaEmergente1 x">
                            <div class="erangeCajaEmergente1Title">
                                Blog
                            </div>
                            <span id="spanCajaEmergente1" class="spanCajaEmergente"><a href="http://blog.campokrasiba.com.mx/" target="_blank">Conoce más sobre Campo Krasiba</a></span>
                        </div>
                    </div>

                    <div class="erangeSquareWrapper">
                        <div class="erangeSquare3">
                            <a href="descargas.html" class="linkBackground"></a>
                        </div>
                        <div class="erangeCajaEmergente3 x">
                            <div class="erangeCajaEmergente1Title">
                                Descargas
                            </div>
                            <span id="spanCajaEmergente2" class="spanCajaEmergente"><a href="descargas.html">Descarga: Mapa de Ubicación, Lista de Requerimientos, Formato de Inscripción</a></span>
                        </div>
                    </div>

                    <div class="erangeSquareWrapper">
                        <div class="erangeSquare4">
                            <a href="galeria.html" class="linkBackground"></a>
                        </div>
                        <div class="erangeCajaEmergente4 x">
                            <div class="erangeCajaEmergente1Title">
                                Album fotográfico
                            </div>
                            <span id="spanCajaEmergente3" class="spanCajaEmergente"><a href="galeria.html">Colección de fotos de nuestros eventos</a></span>
                        </div>
                    </div>

                    <div class="erangeSquareWrapper">
                        <div class="erangeSquare5">
                            <a href="http://campokrasiba.com.mx/tourvirtual/flash/VirtualTour.html" target="_blank" class="linkBackground"></a>
                        </div>
                        <div class="erangeCajaEmergente5 x">
                            <div class="erangeCajaEmergente1Title">
                                Tour virtual
                            </div>
                            <span id="spanCajaEmergente4" class="spanCajaEmergente"><a href="http://campokrasiba.com.mx/tourvirtual/flash/VirtualTour.html" target="_blank">Visita virtual a Campo Krasiba en 360&deg;</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END ROW -->
        

    </section><!-- START BLOG SECTION -->

















