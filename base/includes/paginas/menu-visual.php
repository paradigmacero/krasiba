
	<!-- START WORK SECTION -->
    <section id="menu-visual" class="section">
<!-- 		
        <div class="headline-wrapper">
            <div class="row">
                <h1 class="underline">
					Our Work
				</h1>
                <p class="undertitle">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor cum sociis natoque penatibus et magnis dis parturient montes.
				</p>
            </div>
        </div> -->
		
		
        <div id="work-wrapper">
            <div class="close">
                <a id="close-work-wrapper"></a>
            </div>
			<div id="work-loader"><img src="images/layout/color_loader.gif" alt="" /></div>

			<div id="load-work-content">
			</div>
        </div>
		
		
        <div class="work-container">			

            <div class="row">		
                <ul class="work-tabs">
                    <li class="filter active" data-filter="all">
						<!-- <a>All</a> -->
					</li>
                    <li class="filter" data-filter="webdesign">
						<!-- <a>Webdesign</a> -->
					</li>
                    <li class="filter" data-filter="html">
						<!-- <a>HTML 5 & CSS 3</a> -->
					</li>
                </ul>
            </div>
			
			
       		<!-- START WRAPPER WORK GRID -->
	   	 	<ul id="work-preview">
				
				<!-- START WORK -->
				<li class="mix webdesign">
					<!-- <a data-work-link="#quienes.html"> -->
					<a href="#quienes-somos">
						<img src="base/img/secciones/menu-visual/01.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Quiénes Somos</h4>
							<span class="client">Campo Krasiba fue fundado en el año 2000. Somos un grupo de profesionales de distintas disciplinas, con amplia experiencia en el mundo corporativo y académico.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix html">
					<!-- <a data-work-link="#ubicacion"> -->
					<a href="#ubicacion">
						<img src="base/img/secciones/menu-visual/02.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Ubicación</h4>
                            <span class="client">Campo Krasiba se encuentra a 25 minutos de Santa Fe, en la Cañada de Alférez, zona de la Marquesa, Municipio de Lerma, Estado de México, kilómetro 36.5 de la carretera México – Toluca.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix webdesign">
					<!-- <a data-work-link="#work-03.html"> -->
					<a href="#metodo-krasiba">
						<img src="base/img/secciones/menu-visual/03.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Método Krasiba</h4>
                            <span class="client">Una de las características del Método Krasiba es que no existen dos eventos iguales.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix html">
					<!-- <a data-work-link="#work-04.html"> -->
					<a href="#salon-panoramico">
						<img src="base/img/secciones/menu-visual/04.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Salón Panorámico</h4>
                            <span class="client">Ofrece la posibilidad de celebrar cualquier tipo de reunión enmarcada por la magia del bosque, contando con todas las facilidades didácticas y tecnológicas.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix webdesign">
					<!-- <a data-work-link="#work-05.html"> -->
					<a href="#instalaciones">
						<img src="base/img/secciones/menu-visual/05.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Instalaciones</h4>
                            <span class="client">Campo Krasiba cuenta con un campus de 18,000 m2, alojado en un bosque de 300 hectáreas, que nos permite desplegar nuestros programas en un ambiente de relajación, concentración y contacto con la naturaleza.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix html">
					<!-- <a data-work-link="#work-06.html"> -->
					<a href="#coaching">
						<img src="base/img/secciones/menu-visual/06.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Coaching y Terapia con Caballos</h4>
                            <span class="client">Descubre lo que el coaching con caballos puede hacer por ti y por tu equipo de trabajo.</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix webdesign">
					<!-- <a data-work-link="#work-07.html"> -->
					<a href="#testimonios">
						<img src="base/img/secciones/menu-visual/07.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Testimonios</h4>
                            <span class="client">Opiniones sobre nuestro trabajo</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
				<!-- START WORK -->
				<li class="mix html">
					<!-- <a data-work-link="#work-08.html"> -->
					<a href="#contact">
						<img src="base/img/secciones/menu-visual/08.jpg" alt="" />
						<!-- START LAYER -->
						<div class="layer">
							<h4 class="underline">Contacto</h4>
                            <span class="client">Comunícate con nosotros</span>
						</div><!-- END LAYER -->
					</a>
				</li><!-- END WORK -->
				
                <!-- START WORK -->
                <li class="mix webdesign">
                    <!-- <a data-work-link="#work-09.html"> -->
                    <a href="#work-09">
                        <img src="base/img/secciones/menu-visual/09.jpg" alt="" />
                        <!-- START LAYER -->
                        <div class="layer">
                            <div class="mapaRedes01">
                                <a href="https://plus.google.com/117417186499848167537/posts" target="_blank" class="mapaRedesLink mapaRedesBG1"></a>
                            </div>
                            <div class="mapaRedes02">
                            </div>
                            <div class="mapaRedes03">
                                <a href="http://www.youtube.com/user/campokrasiba" target="_blank" class="mapaRedesLink mapaRedesBG3"></a>
                            </div>

                            <div class="mapaRedes04">
                            </div>
                            <div class="mapaRedes05">
                                <a href="http://mx.linkedin.com/pub/campo-krasiba/3a/39b/4a" target="_blank" class="mapaRedesLink mapaRedesBG2"></a>
                            </div>
                            <div class="mapaRedes06">
                            </div>
                            <div class="mapaRedes06">
                                <a href="https://twitter.com/CampoKrasiba" target="_blank" class="mapaRedesLink mapaRedesBG4"></a>
                            </div>
                            <div class="mapaRedes06">
                            </div>
                            <div class="mapaRedes06">
                                <a href="http://www.facebook.com/CampoKrasiba" target="_blank" class="mapaRedesLink mapaRedesBG5"></a>
                            </div>
                        </div><!-- END LAYER -->
                    </a>
                </li>















			</ul><!-- END WRAPPER WORK GRID -->
			
			<!-- START LOAD MORE BUTTON -->
<!--             
            <div id="work-load-more" class="work-button">
                <a class="button border">
                    Load More
                    <img src="images/layout/color_loader.gif" alt="Loading" />
                </a>
            </div> -->
			
        </div><!-- END WORK CONTAINER -->
		
		
		<!-- START PARALLAX CONTENT -->
<!-- 		
		<div class="parallax-content parallax-clients">
			<div class="row">
				<h3>Our Clients</h3>
				<p class="max-width">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
				</p>
				
			    <ul class="carousel" id="client-carousel">
			        <li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
					<li><a><img src="images/clients/logo.png" alt="" /></a></li>
			    </ul>
			</div>
		</div> -->

        <div>
            <div class="row" style="margin: 20px 0;">
            	&nbsp;
            </div>
        </div>
		
    </section><!-- END WORK SECTION -->
	








