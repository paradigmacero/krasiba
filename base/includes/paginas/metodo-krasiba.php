    <!-- START BLOG SECTION -->
    <section id="metodo-krasiba">
        
        <!-- START WRAPPER -->
        <div class="headline-wrapper">
            <!-- START ROW -->
            <div class="row">
                <h1 class="underline">
                    Método Krasiba
                </h1>
                <p class="undertitle">
                    <!-- xxxxxs -->
                </p>
            </div><!-- END ROW -->
        </div><!-- END WRAPPER -->

        
        <!-- START WRAPPER -->
        <div id="blog-wrapper">
            <div class="close">
                <a id="close-blog-wrapper"></a>
            </div>
            <div id="blog-loader"><img src="images/layout/color_loader.gif" alt="" /></div>

            <div id="load-blog-content">
                <!-- Blog Details load here -->
            </div>
        </div><!-- END WRAPPER -->
        
            
        <!-- START ROW -->
        <div class="row">
            <!-- START BLOG GRID -->
            <ul id="blog-preview">
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-01">
                    <div class="col span_4">
                    </div>                    
                    <div class="col span_8">
                        <span class="detail">
                            Apoyamos a empresas, instituciones académicas y a cualquier tipo de organización en la superación de conflictos internos (confianza, comunicación, mal ambiente laboral, falta de integración y compromiso con las metas corporativas). Una de las características del Método Krasiba es que no existen dos eventos iguales; brindamos capacitación hecha a la medida, siempre diseñamos un plan de actividades de conformidad a los objetivos y problemáticas adecuando su contenido al giro de la empresa, al perfil y número de participantes. Entre los talleres más recurrentes contamos con:
                        </span>
                        <br>
                        <br>
                        <table>
                            <tr>
                                <td>&bull; Liderazgo </td>
                                <td>&bull; Integración </td>
                                <td>&bull; Creatividad </td>
                            </tr>
                            <tr>
                                <td>&bull; Alineación y Balanceo Corporativo </td>
                                <td>&bull; Trabajo en Equipo </td>
                                <td>&bull; Manejo de Crisis </td>
                            </tr>
                            <tr>
                                <td>&bull; Comunicación</td>
                                <td>&bull; Confianza</td>
                                <td>&bull; Planeación </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>&bull; Rompimiento de Paradigmas</td>
                            </tr>
                        </table>
                    </div>
                </li><!-- END BLOG ARTICLE -->

<!-- QUITAR ESTE PÁRRAFO DE ENTRE LOS li. CERRAR EL li ANTERIOR CON UN </UL> Y COMENZAR CON UN <UL> EL SIGUIENTE li -->
<p>EL MÉTODO KRASIBA ESTÁ CONFORMADO POR 7 PUNTOS QUE LOGRAN CONSTRUIR ESTÁ SINERGÍA:</p>
                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-02">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            1.- DECÁLOGO DE LA MONTAÑA
                        </h4>
                        <span class="detail">
                            Integramos los procedimientos, dinámicas, valores y técnicas que permiten grandes logros en la práctica de “Deportes Extremos y de Aventura”, al mundo de las organizaciones inculcando la misma mística entre los miembros de la organización redefiniendo sus límites, algunos de estos valores son:
                            <br>
                            <br>
                            Focalización, trabajo en equipo, entrega, compromiso, cautela y pasión que permiten alcanzar las más altas cimas y profundidades, en condiciones adversas.
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->
                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-03">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            2.- LA “PIRÁMIDE” DE LENCIONI
                        </h4>
                        <span class="detail">
                            Identifica las principales disfunciones al seno de los equipos, como la ausencia de: confianza, conflicto, compromiso, rendición de cuentas y de resultados.
                            <br>
                            <br>
                            Forma parte del Método Krasiba que trabaja directamente, mediante simuladores y dinámicas vivenciales a cada una de ellas, logrando la armonización y marcha óptima del equipo de trabajo.                            
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-04">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            3.- EL COACHING ONTOLÓGICO
                        </h4>
                        <span class="detail">
                            Es una forma diferente de observar la vida, al ser humano y a nuestras relaciones con el entorno y con los demás. Desde el enfoque de la Ontología del lenguaje, se constituye el Coaching Ontológico, se crea este método para auxiliar tanto a personas como a organizaciones a encontrar la mejor manera de fluir, a diseñar y practicar mejores conversaciones, que les permitan coordinar acciones y alcanzar consensos.
                            <br>
                            <br>
                            El Coaching ontológico es para empresas que desean mejorar la comunicación interna de su equipo, e incentivar el liderazgo. Esta poderosa herramienta, complementa el Método Krasiba, permitiendo trabajar en los dominios del cuerpo, el lenguaje y las emociones, logrando una mejor comprensión y equilibrio del ser, ayudándolo a redefinir futuros.                            
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-05">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            4.- COACHING ASISTIDO CON CABALLOS
                        </h4>
                        <span class="detail">
                            Esta técnica de coaching se está extendiendo rápidamente en el mundo, en virtud de su poder de detectar de manera contundente e inmediata los estados de ánimo, las emociones, los estilos de liderazgo y las formas de comunicarnos con los demás.
                            <br>
                            Los caballos tienen la cualidad de mostrarnos en tiempo real las consecuencias de nuestra manera de actuar y de la energía que proyectamos.
                            <br>
                            Si proyecto inseguridad, miedo, agresividad o desconfianza, los animales me devolverán la reacción correspondiente a cada acción o emoción, con la misma intensidad y contundencia con la que yo la entrego.
                            <br>
                            El caballo se vuelve mi espejo y mi aliado, ya que me regala una reacción inmediata cuando mi actitud y mi emoción cambian.
                            <br>
                            El caballo no entiende de jerarquías ni de buenos modales; me devuelve lo que yo le transmito, sin matices ni mensajes cifrados. Es una relación libre de egos y pretensiones.
                            <br>
                            El coach / caballo me muestra el camino hacia la empatía, la confianza, la seguridad, la comunicación auténtica.
                            <br>
                            Me muestra la conveniencia de escuchar, respetar la dignidad del otro y entender su necesidad, y las consecuencias de no hacerlo.
                            <br>
                            Esta danza humano / equino se da entre equipos o individuos y permite ahorrarnos muchos meses de aprendizaje y entrenamiento en la formación de individuos y equipos de alto impacto.                            
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-06">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            5.- CONSCIOUS BUSINESS COACHING
                        </h4>
                        <span class="detail">
                            Este modelo de coaching de negocios diseñado por Fred Kofman, busca la formación de ejecutivos, equipos y empresas conscientes.
                            El modelo lleva a individuos y equipos a transitar de la posición de victimas hacia la de protagonistas de sus propias vidas, que se hacen cargo de sus actos, de sus relaciones y de sus retos de una manera impecable.
                            <br>
                            Se basa en el desarrollo integral del individuo, (integridad esencial, responsabilidad incondicional, humildad ontológica) y de su forma de relacionarse (comunicación auténtica, negociación constructiva, coordinación impecable), desarrollando la máxima destreza: la “Maestría emocional”.
                            <br>
                            Incursiona en los tres dominios fundamentales: La tarea, las relaciones interpersonales y el individuo, logrando entretejer lazos, valores y herramientas, que desarticulan los principales y más recurrentes focos de conflicto en las organizaciones, volviéndolas auténticos foros de realización y trascendencia para sus miembros.
                            <br>
                            El faro central es la búsqueda del “Éxito, más allá del éxito”; lograr resultados en plena congruencia con mis valores.                            
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-07">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            6.- CUERPO Y MOVIMIENTO
                        </h4>
                        <span class="detail">
                            El modelo se enmarca en la corriente del coaching ontológico y honra al cuerpo con toda su magnificencia.
                            <br>
                            El cuerpo sabe, tiene memoria y capacidad de aprendizaje.
                            <br>
                            Por la ventana del cuerpo se accede al alma, al mundo de las emociones y se puede lograr el desarrollo pleno de la persona. Este modelo provee herramientas para entablar un nuevo diálogo con el propio cuerpo y por esa vía, encontrar el “Centro”, desde el cual podemos accionar, interactuar y fluir en el mundo.
                            <br>
                            Maturana postula que lo que distingue al ser humano de otras especies, más allá de su intelecto y su lenguaje, es el diseño de su mano, apta para la caricia.
                            <br>
                            Técnicas como la “Biodanza” y la “Danza primal”, son vehículos para conectar, dialogar y gestionar las propias emociones.                            
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->


                    
                <!-- START BLOG ARTICLE -->
                <li id="mk-li-08">
                    <div class="col span_4">
                    </div>
                    <div class="col span_8">
                        <h4>
                            7.- TEAM COACHING (ARL)
                        </h4>
                        <span class="detail">
                            El Modelo LIM de Team Coaching, se basa en observar y acompañar a los equipos en su accionar habitual, detectar patrones y creencias para llevarlos posteriormente a:
                            <br>
                            • Entenderse y crecer como equipo
                            <br>
                            • Plantearse las preguntas adecuadas 
                            <br>
                            • Reconocer y aceptar al otro, como válido y legítimo 
                            <br>
                            • Complementarse 
                            <br>
                            • Generar espacios de creatividad 
                            <br>
                            • Cambiar la mirada sobre los problemas 
                            <br>
                            • Pensar y aprender juntos 
                            <br>
                            • Escuchar 
                            <br>
                            • Desarrollar una perspectiva sistémica 
                            <br>
                            • Respetar los diferentes enfoques 
                            <br>
                            • Construir sobre las ideas de los demás, mediante reflexión y diálogo 
                            <br>
                            • Dar y recibir feedback 
                            <br>
                            • Pensar en soluciones, más que en problemas.                         
                            <br>
                            Este trabajo tiene un componente importante en aula y se complementa con actividades vivenciales dentro del método Krasiba
                        </span>
                    </div>
                </li><!-- END BLOG ARTICLE -->

            </ul><!-- END BLOG GRID -->
                
            <!-- START LOAD MORE BUTTON -->
<!-- 
            <div id="blog-load-more" class="blog-button">
                <a class="button border">
                    Load More
                    <img src="images/layout/color_loader.gif" alt="Loading" />
                </a>
            </div> -->
                
        </div><!-- END ROW -->
            
            
        <!-- START PARALLAX CONTENT -->
<!--         
        <div class="parallax-content parallax-newsletter">
            <div class="row">
                <h3>Do you like it?</h3>
                <p class="max-width">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                </p>
                <a class="button border">Purchase Now</a>
            </div>
        </div> -->
        
    </section><!-- START BLOG SECTION -->

